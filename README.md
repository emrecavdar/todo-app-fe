# Todo-App

Click for open app: http://104.236.39.102:3005/  .
This is a todo list application. Thanks to this application, users can follow their todos.
## Installation and Setup Instructions

Clone down this repository. You will need node and npm installed globally on your machine.

Installation: 
### `npm install`

Run Tests: 
### `npm test`

To Start Server:
### `npm start`

To Visit App:
### `localhost:3000`