import ENV_LOCAL from "./local.json";
import ENV_DEV from "./dev.json";

export const ENVIRONMENT_NODES={
    LOCAL:"LOCAL",
    DEV:"DEV"
}
const ENV=ENVIRONMENT_NODES.DEV;
const getEnvVariable=()=>{
    switch (ENV){
        case ENVIRONMENT_NODES.LOCAL:
            return ENV_LOCAL;
        case ENVIRONMENT_NODES.DEV:
            return ENV_DEV;
        default:
            return ENV_LOCAL;
    }
}

export const isEnvLocal = () =>
  ENV !== ENVIRONMENT_NODES.PROD &&
  ENV !== ENVIRONMENT_NODES.UAT &&
  ENV !== ENVIRONMENT_NODES.DEV;

export const environment = getEnvVariable();