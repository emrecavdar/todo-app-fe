import React from 'react';

const TodoItem = ({todo, index, clickAction, removeAction}) => {
    return (
        <div>
            <div className={`todo-text ${todo.isCompleted ? 'completed' : ''} `}
                 style={{textDecoration: todo.isCompleted ? "line-through" : ""}} onClick={() => clickAction(index,{id:todo.id,isCompleted:todo.isCompleted,text:todo.text})}>
                {todo.text}

            </div>
            <div>
                <button className={"delete-button"} onClick={() => removeAction(index)}>Delete</button>
            </div>
        </div>

    )
}
export default TodoItem;