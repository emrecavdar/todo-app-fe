import React,{useEffect} from 'react';
import api from '../services/todo.service'
import TodoItem from "./TodoItem";
import {initAxios} from "../services/base.api.service";


const Todo =(todoList)=>{
    const[todos,setTodos]=React.useState([]);
    const[inputValue,setInputValue]=React.useState("");


    useEffect(()=>{
        initAxios()
        api().getTodoList().then((response)=>{
            setTodos(response===null?[]:response);
        });
    },[])
    const addAction=data=>{
        api().addItemToList(data).then((response)=>{
            api().getTodoList().then((response)=>{
                setTodos(response);
            });
        })
    }

    const clickAction=(index,data)=>{

        data.isCompleted=!data.isCompleted
        api().completeItem(index,data).then((response)=>{
            api().getTodoList().then((response)=>{
                setTodos(response);
            });
        })
    }

    const removeAction=index=>{
        api().deleteItem(index).then((response)=>{
            api().getTodoList().then((response)=>{
                setTodos(response===null?[]:response);
            });
        })
    }
    const deleteAll= async()=>{
        await api().deleteAllItem(todos===null?[]:todos)
        setTodos([])
    }

    const handleSubmit=e=>{
        e.preventDefault();
        if (!inputValue) return;
        addAction({id:Math.floor((Math.random() * 1000) + 1),isCompleted:false,text:inputValue})
        setInputValue("")
    }
    console.log(todoList)
    console.log(todos);
    return(

        <div className={`todo-list ${todos===null || todos.length===0 ?'clear':''}`}>
            {
                todoList.todoList &&Array.isArray(todoList.todoList)
                ?
                    todoList.todoList.sort((a,b)=>b.CreateDate-a.CreateDate).map((val,idx)=>(
                        <TodoItem key={idx} index={idx} todo={val.id} clickAction={clickAction} removeAction={removeAction}/>
                    ))
                    :
                    todos.sort((a,b)=>new Date(b.CreateDate)-new Date(a.CreateDate)).map((val,idx)=>(
                        <TodoItem key={idx} index={val.id} todo={val} clickAction={clickAction} removeAction={removeAction}  />
                    ))
            }
            <form onSubmit={handleSubmit} className={"todo-form"} >
                <input type="text" className={"input"} value={inputValue} onChange={e=>setInputValue(e.target.value)}/>
                <button className={"add-button"} type={"submit"}>Add</button>
            </form>
            <button className={"clear-button"} onClick={deleteAll}>Clear</button>
        </div>
    )

}

export default Todo;