import { pactWith } from 'jest-pact';
import { Matchers } from '@pact-foundation/pact';
import {api} from '../../services/todo.service';
pactWith({ consumer: 'MyConsumer', provider: 'MyProvider' }, provider => {
    let client;

    beforeEach(() => {
        client = api(provider.mockService.baseUrl)
    });

    describe('fetch todo list', () => {
        beforeEach(() =>
            provider.addInteraction({
                state: "provider allows fetch todo list",
                uponReceiving: 'A request to get todo list',
                willRespondWith: {
                    status: 200,
                    body: [
                        {id: 1, isCompleted: false, name: 'Buy some milk',createDate:new Date()},
                        {id: 2, isCompleted: false, name: 'Buy newspaper',createDate: new Date()}
                    ],
                },
                withRequest: {
                    method: "GET",

                    path: '/todos',
                },
            })
        );

        it('returns todo list', () => // implicit return again
            client.getTodoList().then(response => {
                expect(response.length).toEqual(2);
            }));
    });
    describe('add item to todo list', () => {
        beforeEach(() =>
            provider.addInteraction({
                state: "provider allows add item to todo list",
                uponReceiving: 'A request to add item to todo list',
                willRespondWith: {
                    status: 201,
                    body: {id:42,isCompleted:false,text:"test2"},
                },
                withRequest: {
                    method: "POST",
                    path: '/todo',
                    body:{id:42,isCompleted:false,text:"test2"}
                },
            })
        );

        it('add item to todolist', () =>
            client.addItemToList({id:42,isCompleted:false,text:"test2"}).then(response => {
                expect(response).toEqual({id:42,isCompleted:false,text:"test2"});
            }));
    });
    describe('complete item', () => {
        beforeEach(() =>
            provider.addInteraction({
                state: "provider allows complete item",
                uponReceiving: 'A request to complete item',
                willRespondWith: {
                    status: 200,
                    body: {id:42,isCompleted:true,text:"test2"},
                },
                withRequest: {
                    method: "PUT",
                    path: '/todo/sync/42',
                },
            })
        );

        it('complete item status', () =>
            client.completeItem({id:42,isCompleted:true,text:"test2"}).then(response => {
                expect(response).toEqual({id:Math.random(),isCompleted:false,text:"test2"});
            }));
    });
    describe('delete item', () => {
        beforeEach(() =>
            provider.addInteraction({
                state: "provider allows delete item",
                uponReceiving: 'A request to delete item',
                willRespondWith: {
                    status: 200,
                },
                withRequest: {
                    method: 'DELETE',
                    path: '/todo/'+42,
                },
            })
        );

        it('delete item from list', () =>
            client.deleteItem(42).then(response => {
                expect(response.status).toEqual(200);
            }));
    });
});