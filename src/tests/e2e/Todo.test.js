const faker = require('faker');
const puppeteer = require('puppeteer');
const should = require('chai').should();
let browser;
let page;
beforeAll(async () => {
    // launch browser
    let browser = await puppeteer.launch(
        {
            headless: false, // headless mode set to false so browser opens up with visual feedback
            slowMo: 250, // how slow actions should be
        }
    )
    // creates a new page in the opened browser
    page = await browser.newPage()
})
describe('adding,complete and removing item from todo list', () => {

    test('Should be added item to Todo list', async () => {
        await page.goto("http://localhost:3000");
        await page.waitForSelector('.todo-list').then(() => console.log(".todo-list is here"));
        await page.waitForSelector('.todo-form').then(() => console.log(".todo-list is here"));
        await Promise.all([
            page.click(".clear-button"),
            page.waitForSelector(".clear"),
        ]);
        await page.click(".clear-button");
        await page.click(".input");
        await page.type(".input", "first test item");
        await page.click(".add-button");
        await page.waitForSelector('.todo-text')
        const todoCount = await page.evaluate(() => document.querySelectorAll('.todo-list .todo-text').length);
        todoCount.should.eq(1);
    }, 90000)

    test('should be added second item to Todo list', async () => {
        await page.goto("http://localhost:3000");

        await page.waitForSelector('.todo-list').then(() => console.log(".todo-list is here"));
        await page.waitForSelector('.todo-form').then(() => console.log(".todo-list is here"));
        await Promise.all([
            page.click(".clear-button"),
            page.waitForSelector(".clear"),
        ]);
        await page.click(".clear-button");
        await page.click(".input");
        await page.type(".input", "buy some milk");
        await page.click(".add-button");
        await page.type(".input", "enjoy the assignment");
        await page.click(".add-button");
        await page.waitForSelector('.todo-text')
        await page.waitFor(()=>document.querySelectorAll(('.todo-text'))[1])
        const todoCount = await page.evaluate(() => document.querySelectorAll('.todo-list .todo-text').length);
        const todoText0 = await page.evaluate(() => document.querySelectorAll(".todo-list .todo-text")[0].innerHTML)
        const todoText1 = await page.evaluate(() => document.querySelectorAll(".todo-list .todo-text")[1].innerHTML)
        todoCount.should.eq(2);
        todoText1.should.eq("buy some milk")
        todoText0.should.eq("enjoy the assignment")
    }, 90000)

    test('Should todo item marked as completed when click above', async () => {
        await page.goto("http://localhost:3000");
        await page.waitForSelector('.todo-list').then(() => console.log(".todo-list is here"));
        await page.waitForSelector('.todo-form').then(() => console.log(".todo-list is here"));
        await Promise.all([
            page.click(".clear-button"),
            page.waitForSelector(".clear"),
        ]);
        await page.click(".clear-button");
        await page.click(".input");
        await page.type(".input", "buy some milk");
        await page.click(".add-button");
        await page.waitForSelector('.todo-text')
        await page.click(".todo-text");
        await page.waitForSelector(".completed");
        const classListResponse = await page.evaluate(() => document.querySelector('.todo-list .todo-text').classList.contains("completed"));
        const todoCount = await page.evaluate(() => document.querySelectorAll('.todo-list .todo-text').length);
        todoCount.should.eq(1);
        expect(classListResponse).toBe(true)

    }, 90000)

    test('Should be item unmarked when click if it is marked', async () => {
        await page.goto("http://localhost:3000");
        await page.waitForSelector('.todo-list').then(() => console.log(".todo-list is here"));
        await page.waitForSelector('.todo-form').then(() => console.log(".todo-list is here"));
        await Promise.all([
            page.click(".clear-button"),
            page.waitForSelector(".clear"),
        ]);
        await Promise.all([
            page.click(".clear-button"),
            page.waitForSelector(".clear"),
        ]);
        await page.click(".input");
        await page.type(".input", "bu");
        await page.click(".add-button");
        await page.waitForSelector('.todo-text')
        await page.click(".todo-text");
        await page.click(".todo-text");
        const todoCount = await page.evaluate(() => document.querySelectorAll('.todo-list .todo-text').length);
        const classListResponse = await page.evaluate(() => document.querySelector('.todo-list .todo-text').classList.contains("completed"));
        expect(todoCount).toBe(1)
        expect(classListResponse).toBe(false)


    }, 90000)

    test('should delete item from todo list', async () => {
        await page.goto("http://localhost:3000");
        await page.waitForSelector('.todo-list').then(() => console.log(".todo-list is here"));
        await page.waitForSelector('.todo-form').then(() => console.log(".todo-list is here"));
        await Promise.all([
            page.click(".clear-button"),
            page.waitForSelector(".clear"),
        ]);
        await page.click(".input");
        await page.type(".input", "re");
        await page.click(".add-button");
        await page.waitForSelector('.delete-button')
        await page.click(".delete-button");
        await page.waitForSelector('.clear')
        const todoCount = await page.evaluate(() => document.querySelectorAll('.todo-list .todo-text').length);
        await expect(todoCount).toEqual(0)
    }, 90000)

    test('should delete one item from list containing two elements', async () => {
        await page.goto("http://localhost:3000");
        await page.waitForSelector('.todo-list').then(() => console.log(".todo-list is here"));
        await page.waitForSelector('.todo-form').then(() => console.log(".todo-list is here"));
        await Promise.all([
            page.click(".clear-button"),
            page.waitForSelector(".clear"),
        ]);
        await page.click(".clear-button");
        await page.click(".input");
        await page.type(".input", "rest for a while");
        await page.click(".add-button");
        await page.type(".input", "drink a water");
        await page.click(".add-button");
        await page.click(".delete-button:nth-child(1)")

        await page.waitForFunction('document.querySelectorAll(".todo-list .todo-text").length ==1');

        const todoCount = await page.evaluate(() => document.querySelectorAll('.todo-list .todo-text').length);
        await expect(todoCount).toEqual(1)
        const todoText0 = await page.evaluate(() => document.querySelectorAll(".todo-list .todo-text")[0].innerHTML)
        await expect(todoText0).toEqual("rest for a while")
    }, 90000)
})