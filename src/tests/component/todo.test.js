import Todo from '../../components/Todo'
import TodoItem from '../../components/TodoItem'
import React from 'react';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import sinon from 'sinon';
import {shallow,configure} from 'enzyme'

configure({adapter:new Adapter()})
describe("<Todo/>",()=>{
    it("Todo component renders the text inside it",()=>{

        const wrapper=shallow(
            <Todo />
        )
        // eslint-disable-next-line jest/valid-expect
        expect(wrapper.find('.todo-list').length).toEqual(1);
        wrapper.unmount();
    })

    it("Todo component renders children correctly",()=>{
        const todoItem=[{
            id:1, isCompleted:false,text:'Buy some milk'
        }]

        const wrapper=shallow(
            <Todo todoList={{todoItem}}/>
        )
        // eslint-disable-next-line jest/valid-expect
        expect(wrapper.find('.todo-list').length).toEqual(1);
        wrapper.unmount();
    })

    it("Todo component should contains todo-form",()=>{
        const todoItem=[{
            id:1, isCompleted:false,text:'Buy some milk'
        },
            {
                id:2, isCompleted:false,text:'3'
            }]

        const wrapper=shallow(
            <Todo todoList={{todoItem}}/>
        )
        // eslint-disable-next-line jest/valid-expect
        expect(wrapper.find('.todo-form').length).toEqual(1);
        wrapper.unmount();
    })


})