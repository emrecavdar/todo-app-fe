import React from 'react';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import {shallow,configure} from 'enzyme'
import TodoItem from "../../components/TodoItem";
import sinon from "sinon";


configure({adapter:new Adapter()})
describe("<TodoItem/>",()=> {
    const todoItem=[{
        id:1, isCompleted:false,text:'Buy some milk'
    },
        {
            id:2, isCompleted:false,text:'3'
        }]
    const clickAction=sinon.spy();
    const removeAction=sinon.spy();

    it("TodoItemComponent should renders correctly", () => {

        const component = shallow(<TodoItem todo={todoItem} index={Math.random()} clickAction={clickAction} removeAction={removeAction}/>);
        expect(component.getElements()).toMatchSnapshot();
    })

});