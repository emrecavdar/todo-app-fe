import Axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

import { environment } from '../environment';
import http from 'axios/lib/adapters/http'
var axiosInstance
var isTokenRefreshing


export const initAxios = () => {
    axiosInstance = Axios.create({
        baseURL: environment.baseApiURL,
        timeout: 60000,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',

        },
        adapter:http
    });

    axiosInstance.interceptors.response.use(
        (response) => {
            return response.data;
        },
        (err) => {
            // if (err.response && err.response.status === 403) return AuthUserService.forceLogout();
            return Promise.reject({
                ...err,
                ...err.response,
                status: err.status
            });
        }
    );
}


const middlewareAPIGateway = (apiCall) => {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await apiCall();
            resolve(response);
        } catch (error) {
            reject(error);
        }
    });
}

const httpGetFNC = (URL) => {
    return middlewareAPIGateway(() => {
        console.log(axiosInstance)
        return axiosInstance.get(URL).catch(handleError)
    });
}

const httpPostFNC = (URL, body) => {
    return middlewareAPIGateway(() => {
        return axiosInstance.post(URL, body && typeof body === 'object' ? JSON.stringify(body) : body).catch(handleError);
    });
}


const httpPutFNC = (URL, body) => {
    return middlewareAPIGateway(() => {
        return axiosInstance.put(URL, JSON.stringify(body)).catch(handleError);
    });
}

const httpDeleteFNC = (URL) => {
    return middlewareAPIGateway(() => {
        return axiosInstance.delete(URL).catch(handleError)
    });
}

const handleError = (err) => {
    console.log('API_ERR', err);
    return Promise.reject(err && err.data ? err.data : { message: 'UNKNOW' });
}




export const apiService = {
    httpGetFNC,
    httpPostFNC,
    httpPutFNC,
    httpDeleteFNC,
}

