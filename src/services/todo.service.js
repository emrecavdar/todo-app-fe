import axios from 'axios'
import {environment} from '../environment'
import http from 'axios/lib/adapters/http'
import { apiService } from "./base.api.service";

 const api=()=>({
    getTodoList:()=> {
      return apiService.httpGetFNC("todos")
    },
    addItemToList:(data)=>{
        return apiService.httpPostFNC("todo",data)
    },
    completeItem:(index,data)=>{
       return  apiService.httpPutFNC(`todo/sync/${index}`,data)
    },
    deleteItem:(index)=>{
       return apiService.httpDeleteFNC(`todo/${index}`)
    },
    deleteAllItem:(data)=>{
        data.map((val)=>{
            apiService.httpDeleteFNC(`todo/${val.id}`)
        })
    }
})
export default api;